import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/lesson1',
    name: 'lesson1',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Lesson1.vue')
  },
  {
    path: '/lesson2',
    name: 'lesson2',
    component: () => import( '../views/Lesson2.vue')
  },
  {
    path: '/lesson3',
    name: 'lesson3',
    component: () => import( '../views/Lesson3.vue')
  },
  {
    path: '/lesson4',
    name: 'lesson4',
    component: () => import( '../views/Lesson4.vue')
  },
  {
    path: '/calculyator',
    name: 'calc',
    component: () => import( '../views/Calculyator.vue')
  },
  {
    path: '/lesson-localstorage',
    name: 'lesson5',
    component: () => import( '../views/LessonLocalstorage.vue')
  },
  {
    path: '/second-localstrorage',
    name: 'lesson6',
    component: () => import( '../views/secondLocalstorege.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
